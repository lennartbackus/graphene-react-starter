def test_auth_success(client, user_factory):
    user_factory.create_user(
        username='test_user', password='test_password',
    )
    response = client.post(
        '/auth', json={
            'username': 'test_user',
            'password': 'test_password',
        }, content_type='application/json',
    )
    assert response.status_code == 200


def test_auth_invalid_credentials(client, user_factory):
    user_factory.create_user(
        username='test_user', password='test_password',
    )
    response = client.post(
        '/auth', json={
            'username': 'wrong_user',
            'password': 'test_password',
        }, content_type='application/json',
    )
    assert response.status_code == 401
    response = client.post(
        '/auth', json={
            'username': 'test_user',
            'password': 'wrong_password',
        }, content_type='application/json',
    )
    assert response.status_code == 401

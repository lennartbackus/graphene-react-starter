def test_whoami_success(client, user_factory):
    user_factory.create_user(
        username='test_user',
        password='test_password',
        email='test_user@company.com',
    )
    response = client.post(
        '/auth', json={
            'username': 'test_user',
            'password': 'test_password',
        }, content_type='application/json',
    )
    token = response.json['access_token']
    response = client.get('/whoami', headers={'Authorization': f'JWT {token}'})
    assert response.json == {
        'id': 1,
        'username':
        'test_user',
        'admin': False,
        'email': 'test_user@company.com',
    }

from models import User


def test_register_invalid_content_type(client):
    response = client.post('/register')
    assert response.status_code == 400
    assert response.json['errors'][0] == \
        'Invalid content-type. Must be application/json.'


def test_register_missing_data(client):
    response = client.post(
        '/register',
        json={'username': 'test_user'},
        content_type='application/json',
    )
    assert response.status_code == 400
    assert response.json['errors'][0] == \
        "Missing required data 'password'"


def test_register_invalid_data_type(client):
    response = client.post(
        '/register',
        json={'username': 'test_user', 'password': 1},
        content_type='application/json',
    )
    assert response.status_code == 400
    assert response.json['errors'][0] == \
        "Wrong data format 'password' is of type 'int' but expected 'str'"


def test_register_success(client, session):
    assert session.query(User).count() == 0
    response = client.post(
        '/register', json={
            'username': 'test_user',
            'password': 'test_password',
        }, content_type='application/json',
    )
    assert response.status_code == 201
    assert session.query(User).count() == 1


def test_register_duplicate_username(client, user_factory):
    user_factory.create_user(username='user1')
    response = client.post(
        '/register',
        json={'username': 'user1', 'password': 'test_password'},
        content_type='application/json',
    )
    assert response.status_code == 400
    assert response.json['errors'][0] == 'Username already taken'

import pytest
from app.authentication.password_hasher import hash_password
from models import User


class UserFactory:

    def __init__(self, session):
        self.session = session

    def create_user(
        self,
        username: str = 'TestUser',
        password: str = 'TestPassword',
        email: str = 'test@test.com',
    ):
        new_user = User(
            username=username,
            password=hash_password(password),
            email=email,
        )
        self.session.add(new_user)
        self.session.flush()
        return new_user


@pytest.fixture(scope='function')
def user_factory(session):
    return UserFactory(session)

import setuptools

with open('README.md') as fh:
    long_description = fh.read()

setuptools.setup(
    name='graphene-react-starter',
    version='0.0.1',
    author='Lennart Backus',
    author_email='lennartbackus@gmail.com',
    description='Starter for web projects with React frontend and Graphene backend',
    url='https://gitlab.com/lennartbackus/graphene-react-starter',
    package_dir={'': 'src'},
    packages=setuptools.find_packages(where='src'),
    install_requires=[
        'Flask==1.1.2',
        'Flask-JWT==0.3.2',
        'SQLAlchemy==1.3.22',
        'Flask-SQLAlchemy==2.4.4',
        'Flask-Migrate==2.5.3',
        'Flask-Script==2.0.6',
        'graphene==2.1.8'
    ],
    setup_requires=[
        'pytest-runner==5.2',
    ],
    tests_require=[
        'pytest==6.2.1',
        'requests==2.25.1',
        'pre-commit==2.9.3',
    ],
)

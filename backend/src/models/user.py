from app.database import db
from sqlalchemy import Boolean
from sqlalchemy import Column
from sqlalchemy import Integer
from sqlalchemy import Sequence
from sqlalchemy import String


class User(db.Model):
    __tablename__ = 'users'

    id = Column(Integer, Sequence('users_id_seq'), primary_key=True)
    username = Column(String, nullable=False, unique=True)
    password = Column(String, nullable=False)
    admin = Column(Boolean, default=False, nullable=False)
    email = Column(String)

    def as_dict(self):
        exclude = [
            'password',
        ]
        return {c.name: getattr(self, c.name) for c in self.__table__.columns if c.name not in exclude}  # noqa

from werkzeug.exceptions import BadRequest


def get_value(data, key, expected_type, required):

    if key not in data.keys():
        if required:
            raise BadRequest(f"Missing required data '{key}'")
        return

    value = data[key]

    if type(value) != expected_type:
        raise BadRequest(
            "Wrong data format '{}' is of type '{}' but expected '{}'".format(
                key,
                type(value).__name__,
                expected_type.__name__,
            ),
        )

    return value

import os
""" Main Flask App definition """
from flask import Flask, render_template
from app import database
from app.authentication.jwt import setup_jwt
from app.blueprints import graphql_blueprint
from app.prefix_middleware import PrefixMiddleware

db = database.db

def create_app(
    config_name: str = os.getenv(
        'BACKEND_CONFIG', 
        'app.config.DevelopmentConfig'
    ),
):
    """ Create the Flask app object and set it up.

    Reads BACKEND_CONFIG env variable for config settings
    defaults to 'app.config.DevelopmentConfig' is none is supplied.
    """
    app = Flask(__name__)

    app.config.from_object(config_name)

    db.init_app(app)

    app.wsgi_app = PrefixMiddleware(app.wsgi_app, prefix='/api')

    app.register_blueprint(graphql_blueprint, url_prefix='/graphql')

    setup_jwt(app)

    return app

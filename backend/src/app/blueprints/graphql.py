""" Blueprint for GraphQL endpoints

Generates all GraphQL related endpoints.
/graphql            Main GraphQL endpoint and GraphiQL interface
/graphql/batch      Batch query endpoint for apollo client
"""

from flask import Blueprint
from flask_graphql import GraphQLView
from flask_jwt import jwt_required
from schema import schema

graphql_blueprint = Blueprint(
    'graphql_blueprint',
    __name__,
)

@graphql_blueprint.before_request
@jwt_required()
def before_request_func():
    """ Add JWT authentication to all calls in blueprint """

graphql_blueprint.add_url_rule(
    '/',
    view_func=GraphQLView.as_view(
        'graphql',
        schema=schema,
        graphiql=False,
    ),
    strict_slashes = False
)

graphql_blueprint.add_url_rule(
    '/batch',
    view_func=GraphQLView.as_view(
        'graphql_batch',
        schema=schema,
        batch=True
    ),
    strict_slashes = False
)

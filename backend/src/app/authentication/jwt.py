import json

from app.authentication.password_hasher import hash_password
from app.authentication.password_hasher import verify_password
from app.database import db
from app.validation import get_value
from flask import request
from flask import Response
from flask_jwt import current_identity
from flask_jwt import JWT
from flask_jwt import jwt_required
from models import User
from sqlalchemy.exc import IntegrityError
from werkzeug.exceptions import BadRequest


def register_user_in_db(username, password, email):
    new_user = User(
        username=username,
        password=hash_password(password),
        email=email,
    )
    db.session.add(new_user)
    db.session.commit()


def authenticate(username, password):
    user = User.query.filter(User.username == username).one_or_none()
    if user and verify_password(user.password, password):
        return user


def identity(payload):
    return User.query.filter(User.id == payload['identity']).one_or_none()


def setup_jwt(app):
    JWT(app, authenticate, identity)

    @app.route('/whoami', methods=['GET'])
    @jwt_required()
    def whoami():
        return Response(
            json.dumps(current_identity.as_dict()),
            status=200,
            mimetype='application/json',
        )

    @app.route('/register', methods=['POST'])
    def register():
        if request.content_type != 'application/json':
            return Response(
                '{"errors": ["Invalid content-type. '
                'Must be application/json."]}',
                status=400,
                mimetype='application/json',
            )

        try:
            username = get_value(request.json, 'username', str, True)
            password = get_value(request.json, 'password', str, True)
            email = get_value(request.json, 'email', str, False)
        except BadRequest as error:
            return Response(
                '{"errors": ["%s"]}' % error.description,
                status=400,
                mimetype='application/json',
            )

        try:
            register_user_in_db(username, password, email)
        except IntegrityError:
            return Response(
                '{"errors": ["Username already taken"]}',
                status=400,
                mimetype='application/json',
            )

        return Response('{}', status=201, mimetype='application/json')

import os

basedir = os.path.abspath(os.path.dirname(__file__))

class Config:
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True
    SECRET_KEY = os.getenv('BACKEND_SECRET', 'super_local_dev_secret')
    SQLALCHEMY_DATABASE_URI = os.getenv(
        'BACKEND_DATABASE', 'sqlite:///../../../backend.db',
    )
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class DevelopmentConfig(Config):
    DEBUG = True


class TestConfig(Config):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = 'sqlite://'

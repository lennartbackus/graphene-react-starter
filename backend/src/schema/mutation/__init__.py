import graphene

class RootMutation(graphene.ObjectType):
    ok = graphene.Boolean()
    
    def resolve_ok(root, info, **kwargs):
        return True


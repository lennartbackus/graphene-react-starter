""" Graphene schema that is exposed to the API """
import graphene
from schema.mutation import RootMutation
from schema.query import RootQuery
from schema.subscription import RootSubscription

schema = graphene.Schema(
    query=RootQuery,
    mutation=RootMutation,
    subscription=RootSubscription
)

""" Load RootQuery and extend it with availible modules """
from .modules import *
from .base_root_query import BaseRootQuery as RootQuery

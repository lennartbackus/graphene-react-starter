"""
BaseRootQuery is the template for the RootQuery class.
Entrypoints are added using the @root_query decorator.
"""
import graphene

class BaseRootQuery(graphene.ObjectType):
    """ BaseRootQuery object with health check """
    ok = graphene.Boolean()

    def resolve_ok(root, info, **kwargs):
        return True

def root_query(name, field_type):
    """
    Decorator for RootQuery modules
    Functions using this decorator are autmatcally added to
    the RootQuery class at runtime.
    """
    def wrap(f):
        BaseRootQuery._meta.fields.update({name: graphene.Field(field_type)})
        setattr(BaseRootQuery, name, field_type)
        setattr(BaseRootQuery, f'resolve_{name}', f)
        return f
    return wrap

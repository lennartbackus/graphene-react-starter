""" Exaple root query entrypoint { now }

Demonstration of how to apply the

   @root_quey(name, field_type)

decorator
"""
import graphene
from datetime import datetime
from schema.query.base_root_query import root_query

@root_query('now', graphene.DateTime)
def now_resolver(root, info, **kwargs):
    """ Resolve function for 'now' query field """
    return datetime.now()

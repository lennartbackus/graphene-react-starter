## The Financial Game Backend

The key framework packages:

- Flask
- Flask-JWT
- SQLAlchemy
- Graphene
- Flask-GraphQL

# Graphene React Starter

## Setting up

### Backend

Edit `backend/setup.py` to contain project information.

[optional/recomended] create python virtualenv

Install module: enter `backend` folder and run `pip install -e .`.

By default running the backend with create a development database called backend.db in the root folder. If another database is desired
set the envoirement variable `BACKEND_DATABASE` to a valid SQLAlchemy connection string.

To initialise the database: enter the `backend` folder and run `python manage.py db upgrade`.

### Frontend

Edit `frontend/

## Running & Testing

### Backend 

Running: enter `backend` folder and run `flask run`.

Testing: enter `backend` folder and run `python setup.py test`.

### Frontend
import React from 'react';
// eslint-disable-next-line
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Dashboard from './views/Dashboard';
import AuthenticationView from './views/AuthenticationView';
import HaNav from './components/nav/HaNav';
import { PrivateRoute } from './_helpers/private-route';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
    },
    toolbar: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-end',
      padding: theme.spacing(0, 1),
      // necessary for content to be below app bar
      ...theme.mixins.toolbar,
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
    },
  }),
);

export default function App(props) {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <HaNav></HaNav>
      <main className={classes.content}>
        <div className={classes.toolbar} />
        <Route path="/login" exact component={ AuthenticationView } />
        <PrivateRoute path="/" exact component={ Dashboard } />
      </main>
    </div>
  );
}
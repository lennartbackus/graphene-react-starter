import { BehaviorSubject } from 'rxjs';
import { handleResponse } from '../_helpers/handle-response';

const tokenSubject = new BehaviorSubject(localStorage.getItem('token') || "");

export const authenticationService = {
    login,
    logout,
    refresh,
    token: tokenSubject.asObservable(),
    get tokenValue () { return tokenSubject.value }
};

function login(username, password) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ username, password })
    };

    return fetch(`http://localhost:3000/api/auth`, requestOptions)
        .then(handleResponse)
        .then(data => {
            console.log("LOGIN")
            console.log(data);
            localStorage.setItem('token', data.access_token);
            tokenSubject.next(data.access_token);
            return data.access_token;
        });
}

function logout() {
    // remove user from local storage to log user out
    console.log("LOGOUT")
    localStorage.removeItem('token');
    tokenSubject.next("");
}

function refresh() {
    return fetch(`http://localhost:3000/api/whoami`, { 
        headers: {
            'Authorization': `Bearer ${tokenSubject.value}`
        }
    })
    .then(handleResponse)
    .then(data => {
        console.log("REFRESH")
        console.log(data);
        // localStorage.setItem('token', JSON.stringify(data.token));
        // tokenSubject.next(data.token);
        // return data.token;
    });
}
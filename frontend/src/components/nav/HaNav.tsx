import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import HaAppBar from './HaAppBar'
import HaDrawer from './HaDrawer'


export default function HaNav() {
  const [open, setOpen] = React.useState(false);

  return (
    <div className="HaNav">
        <CssBaseline />
        <HaAppBar open={open} setOpen={setOpen}></HaAppBar>
        <HaDrawer open={open}></HaDrawer>
    </div>
  );
}
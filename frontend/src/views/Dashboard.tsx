import { useQuery, gql } from '@apollo/client';

const TEST_QUERY = gql`
  query Test {
    ok
  }
`;

export default function Dashboard() {
  const { loading, error, data } = useQuery(TEST_QUERY);
  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error :(</p>;
  console.log(data);
  return (
    <div>
      <h1>Dashboard</h1>
      {data.ok?
        <p>GraphQL works!</p>
      :
        <p>GraphQL does not work :(</p>
      }
    </div>
  );
}